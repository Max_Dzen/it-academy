var countries = [
    {name:'a', capital: '1' },
    {name:'b', capital: '2' },
    {name:'c', capital: '3' },
    {name:'d', capital: '4' },
    {name:'e', capital: '5' }
];

function askCountry() {
    return prompt('введите название страны');
};

function askCapital() {
    return prompt('введите название столицы');
};

function addCountry() {
    var country = {};
    country.name = askCountry();
    country.capital = askCapital();
    countries.push(country);
};

function showCountries() {
    console.log(countries);
};

function getCountryInfo() {
    var countryName = askCountry();
    var requestedCountry = countries.find(function(country){
        return countryName === country.name;
    });

    if (requestedCountry) {
        console.log('Столица: ' + requestedCountry.capital);
    } else {
        console.log('Нет информации о стране');
    }
};

function deleteCountry() {
    var countryDel = askCountry();
    var removableCountry = countries.filter(function(country){
        return countryDel !== country.name;
    });
    
    if (countries.length === removableCountry.length) {
        console.log('Страна ' + countryDel + ' не найдена!');
    } else {
        console.log('Страна ' + countryDel + ' удалена!');
    }
};

var enterInfoBtn = document.querySelector('button:nth-child(1)');
console.log(enterInfoBtn);
enterInfoBtn.addEventListener('click', addCountry);

var getCountryInfoBtn = document.querySelector('button:nth-child(2)');
console.log(getCountryInfoBtn);
getCountryInfoBtn.addEventListener('click', getCountryInfo);

var showCountriesBtn = document.querySelector('button:nth-child(3)');
console.log(showCountriesBtn);
showCountriesBtn.addEventListener('click', showCountries);

var deleteCountryBtn = document.querySelector('button:nth-child(4)');
console.log(deleteCountryBtn);
deleteCountryBtn.addEventListener('click', deleteCountry);